/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.avian.chat;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.avian.chat";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "experimentalPlay";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.2";
  public static final String FLAVOR_app = "experimental";
  public static final String FLAVOR_type = "play";
  // Fields from product flavor: experimental
  public static final boolean IS_OFFICIAL = false;
  // Fields from product flavor: play
  public static final boolean FDROID_BUILD = false;
}
