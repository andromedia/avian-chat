import React from "react";
import isEqual from "lodash/isEqual";
import PropTypes from "prop-types";
import RNFetchBlob from "rn-fetch-blob";
import Image from "./Image";
import Audio from "./Audio";
import CameraRoll from "@react-native-community/cameraroll";
import Video from "./Video";
import Reply from "./Reply";
import { formatAttachmentUrl } from "../../lib/utils";
import {
  Share,
  Button,
  View,
  Text,
  PermissionsAndroid,
  Alert,
} from "react-native";
import { useContext } from "react";
import MessageContext from "./Context";

const Attachments = React.memo(
  ({ attachments, timeFormat, showAttachment, getCustomEmoji, theme }) => {
    if (!attachments || attachments.length === 0) {
      return null;
    }

    return attachments.map((file, index) => {
      if (file.image_url) {
        return (
          <Image
            key={file.image_url}
            file={file}
            showAttachment={showAttachment}
            getCustomEmoji={getCustomEmoji}
            theme={theme}
          />
        );
      }
      if (file.audio_url) {
        return (
          <Audio
            key={file.audio_url}
            file={file}
            getCustomEmoji={getCustomEmoji}
            theme={theme}
          />
        );
      }
      if (file.video_url) {
        return (
          <Video
            key={file.video_url}
            file={file}
            showAttachment={showAttachment}
            getCustomEmoji={getCustomEmoji}
            theme={theme}
          />
        );
      }

      if (file.title) {
        //file open
        const fileExtension = file.title.substr(
          file.title.lastIndexOf("."),
          file.title.length - 1
        );

        if (
          fileExtension === ".pdf" ||
          fileExtension === ".doc" ||
          fileExtension === ".docx" ||
          fileExtension === ".ppt" ||
          fileExtension === ".pptx"
        ) {
          const { baseUrl, user } = useContext(MessageContext);
          const fileUrl = formatAttachmentUrl(
            file.title_link,
            user.id,
            user.token,
            baseUrl
          );
          return (
            <View
              style={{
                backgroundColor: "#00000000",
              }}
            >
              <Button
                title={file.title + " "}
                style={{
                  color: "white",
                }}
                onPress={async () => {
                  try {
                    downloadFile(fileUrl);
                  } catch (err) {
                    alert(err);
                  }
                }}
              >
                <Text>{file.title + " "}</Text>
              </Button>
              {file.description && (
                <Text
                  style={{
                    textAlign: "left",
                    color: theme === "dark" ? "white" : "black",
                  }}
                >
                  {file.description}
                </Text>
              )}
            </View>
          );
        }
      }

      // eslint-disable-next-line react/no-array-index-key
      return (
        <Reply
          key={index}
          index={index}
          attachment={file}
          timeFormat={timeFormat}
          getCustomEmoji={getCustomEmoji}
          theme={theme}
        />
      );
    });
  },
  (prevProps, nextProps) =>
    isEqual(prevProps.attachments, nextProps.attachments) &&
    prevProps.theme === nextProps.theme
);

Attachments.propTypes = {
  attachments: PropTypes.array,
  timeFormat: PropTypes.string,
  showAttachment: PropTypes.func,
  getCustomEmoji: PropTypes.func,
  theme: PropTypes.string,
};
Attachments.displayName = "MessageAttachments";

export default Attachments;

const downloadFile = async (url) => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      actualDownload(url);
    } else {
      Alert.alert(
        "Permission Denied!",
        "You need to give storage permission to download the file"
      );
    }
  } catch (err) {
    console.warn(err);
  }
};

const actualDownload = (file) => {
  RNFetchBlob.config({
    fileCache: true,
    addAndroidDownloads: {
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      path: `${RNFetchBlob.fs.dirs.DownloadDir}/${file}`,
    },
  })
    .fetch("GET", file, {})
    .then((res) => {
      console.log("The file saved to ", res.path());
    })
    .catch((e) => {
      console.log(e);
    });
};

const onShare = async (props) => {
  try {
    const result = await Share.share({
      url: props,
    });
    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        return;
      } else {
        return;
      }
    } else if (result.action === Share.dismissedAction) {
      return;
    }
  } catch (error) {
    alert(error.message);
  }
};
